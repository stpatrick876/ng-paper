/*
 * Public API Surface of ng-paper
 */

export * from './lib/ng-paper.module';
export * from './lib/ng-paper-core/ng-paper-core.module';

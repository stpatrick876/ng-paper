import { NgModule } from '@angular/core';
import { NgPaperCoreModule } from './ng-paper-core/ng-paper-core.module';
@NgModule({
  declarations: [],
  imports: [NgPaperCoreModule],
  exports: []
})
export class NgPaperModule {}

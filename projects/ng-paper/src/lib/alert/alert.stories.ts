import { CommonModule } from '@angular/common';
import { moduleMetadata } from '@storybook/angular';
import { AlertComponent as Alert } from './alert.component';
import { ButtonComponent as Button } from '../ng-paper-core/button/button.component';
import { NgPaperCoreModule } from '../ng-paper-core/ng-paper-core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AlertService } from './alert.service';
import { AlertDemoComponent } from './alert-demo.component';
import { PaperType } from '../models';

export default {
  title: 'Paper Alert',
  component: Alert,
  decorators: [
    moduleMetadata({
      declarations: [Alert, AlertDemoComponent],
      imports: [CommonModule, NgPaperCoreModule],
      providers: [AlertService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
  ]
};
export const Alerts = () => ({
  template: `
  <div style="padding: 3rem;">
    <ng-paper-alert></ng-paper-alert>
     <ng-paper-alert-demo></ng-paper-alert-demo>
   </div>
  `
});

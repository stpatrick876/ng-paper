import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import pullAt from 'lodash/pullAt';
import { PaperType } from '../models';
// import { TitleCasePipe } from '@angular/common';

export interface AlertConfig {
  type: Exclude<PaperType, PaperType.primary | PaperType.muted>;
  title?: string;
  content: string;
  dismissible?: boolean;
}
@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private _alerts = new BehaviorSubject<AlertConfig[]>([]);
  private dataStore: { alerts: AlertConfig[] } = { alerts: [] };
  public readonly alerts = this._alerts.asObservable();

  constructor() {}

  /**
   * Adds an alert to list of alert to be displayed
   */
  addAlert(alert: AlertConfig) {
    alert.title = alert.title || alert.type;
    alert.dismissible = alert.dismissible || false;
    this.dataStore.alerts.push(alert);
    const { alerts } = { ...this.dataStore };
    this._alerts.next(alerts);
  }

  removeAlertAt(index: number) {
    const { alerts } = { ...this.dataStore };
    pullAt(alerts, [index]);
    this._alerts.next(alerts);
  }

  get activeAlertsCount() {
    return this.dataStore.alerts.length;
  }
}

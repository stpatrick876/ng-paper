import { PaperType } from './../models';
import { Component, OnInit, Input } from '@angular/core';
import { AlertConfig, AlertService } from './alert.service';

@Component({
  selector: 'ng-paper-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  PaperType = PaperType;
  alerts: AlertConfig[] = [];
  alertClasses = {
    record1showLifts: true
  };
  constructor(private _alertService: AlertService) {}

  ngOnInit() {
    this._alertService.alerts.subscribe((alerts: AlertConfig[]) => {
      this.alerts = alerts;
    });
  }

  getAlertClass(alert: AlertConfig) {
    const { type, dismissible } = alert;
    return `ng-paper-alert--${PaperType[type]} ${dismissible &&
      '--dismissible'}`;
  }

  onDismissAlert(index: number) {
    this._alertService.removeAlertAt(index);
  }
}

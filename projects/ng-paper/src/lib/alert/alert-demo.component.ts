import { AlertService } from './alert.service';
import { Component } from '@angular/core';
import { PaperType } from '../models';

@Component({
  selector: 'ng-paper-alert-demo',
  template: `
    <div style="display: flex;justify-content: space-evenly">
      <ng-paper-button
        [text]="'Launch Success Alert'"
        (onClick)="
          onLaunchAlert({
            type: PaperType.success,
            dismissible: true
          })
        "
      ></ng-paper-button>
      <ng-paper-button
        [text]="'Launch Info Alert'"
        (onClick)="
          onLaunchAlert({
            type: PaperType.secondary,
            dismissible: true
          })
        "
      ></ng-paper-button>

      <ng-paper-button
        [text]="'Launch Warning Alert'"
        (onClick)="
          onLaunchAlert({
            type: PaperType.warning,
            dismissible: true
          })
        "
      ></ng-paper-button>

      <ng-paper-button
        [text]="'Launch Danger Alert'"
        (onClick)="
          onLaunchAlert({
            type: PaperType.danger,
            dismissible: true
          })
        "
      ></ng-paper-button>
    </div>
  `
})
export class AlertDemoComponent {
  PaperType = PaperType;

  constructor(private _alertService: AlertService) {}

  onLaunchAlert(config: any) {
    const { type, dismissible } = config;
    this._alertService.addAlert({
      type,
      content: `My ${PaperType[type]} alert ${this._alertService
        .activeAlertsCount + 1}`,
      dismissible
    });
  }
}

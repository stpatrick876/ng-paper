import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { BadgeComponent } from './badge/badge.component';
import { TextComponent } from './text/text.component';

@NgModule({
  declarations: [ButtonComponent, BadgeComponent, TextComponent],
  imports: [CommonModule],
  exports: [ButtonComponent]
})
export class NgPaperCoreModule {}

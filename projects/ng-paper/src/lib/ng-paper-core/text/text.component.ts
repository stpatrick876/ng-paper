import { Component, OnInit, Input } from '@angular/core';

export enum textType {
  h1,
  h2,
  h3,
  h4,
  h5,
  h6
}

@Component({
  selector: 'ng-paper-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  @Input() text: string;
  @Input() type: textType;

  constructor() {}

  ngOnInit() {}
}

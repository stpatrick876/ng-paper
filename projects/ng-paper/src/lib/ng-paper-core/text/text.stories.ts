import { TextComponent as Text } from './text.component';
import { moduleMetadata } from '@storybook/angular';

export default {
  title: 'Paper Text',
  component: Text,
  decorators: [
    moduleMetadata({
      declarations: [Text]
    })
  ]
};

export const headings = () => ({
  component: Text,
  template: `
      <ng-paper-text text="Heading 1" type="h1"></ng-paper-text>
      <br/>
      <ng-paper-text text="Heading 2" type="h2"></ng-paper-text>
      <br/>
      <ng-paper-text text="Heading 3" type="h3"></ng-paper-text>
      <br/>
      <ng-paper-text text="Heading 4" type="h4"></ng-paper-text>
      <br/>
      <ng-paper-text text="Heading 5" type="h5"></ng-paper-text>
      <br/>
      <ng-paper-text text="Heading 6" type="h6"></ng-paper-text>
  `
});

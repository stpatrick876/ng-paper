import { Component, OnInit, Input } from '@angular/core';

export enum badgeHeading {
  h1,
  h2,
  h3,
  h4,
  h5,
  h6
}

enum badgeType {
  primary,
  secondary,
  success,
  warning,
  danger,
  muted
}
@Component({
  selector: 'ng-paper-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {
  @Input() label: string;
  @Input() content: string;
  @Input() heading: badgeHeading;
  @Input() type: badgeType;

  constructor() {}

  ngOnInit() {}

  getBadgeClass() {
    const { type } = this;
    let className = 'paper-badge';

    if (type) {
      className = `${className} ${type}`;
    }

    return className;
  }
}

import { moduleMetadata } from '@storybook/angular';

import { BadgeComponent as Badge } from './badge.component';

export default {
  title: 'Paper Badge',
  component: Badge,
  decorators: [
    moduleMetadata({
      declarations: [Badge]
    })
  ]
};

export const Basic = () => ({
  component: Badge,
  props: {
    content: '100'
  }
});

export const WithLabel = () => ({
  template: `
  <ng-paper-badge label="Example h1 heading  "  heading="h1" content="100"></ng-paper-badge>
  <ng-paper-badge label="Example h2 heading  "  heading="h2" content="100"></ng-paper-badge>
  <ng-paper-badge label="Example h3 heading  "  heading="h3" content="100"></ng-paper-badge>
  <ng-paper-badge label="Example h4 heading  "  heading="h4" content="100"></ng-paper-badge>
  <ng-paper-badge label="Example h5 heading  "  heading="h5" content="100"></ng-paper-badge>
  <ng-paper-badge label="Example h6 heading  "  heading="h6" content="100"></ng-paper-badge>

  `,
  props: {
    label: 'Labeled Badge',
    content: '100'
  }
});

export const Colors = () => ({
  template: `
          <ng-paper-badge label="Primary Badge  " heading="h3" content="100" type="primary"></ng-paper-badge>
          <ng-paper-badge label="Secondary Badge  " heading="h3" content="100" type="secondary"></ng-paper-badge>
          <ng-paper-badge label="Warning Badge  " heading="h3" content="100" type="warning"></ng-paper-badge>
          <ng-paper-badge label="Danger Badge  " heading="h3" content="100" type="danger"></ng-paper-badge>
          <ng-paper-badge label="Muted Badge  " heading="h3" content="100" type="muted"></ng-paper-badge>
  `
});

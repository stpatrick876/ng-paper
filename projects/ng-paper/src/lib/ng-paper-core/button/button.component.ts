import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

enum btnSize {
  small,
  large,
  block
}
enum buttonType {
  primary,
  secondary,
  success,
  warning,
  danger,
  muted
}
@Component({
  selector: 'ng-paper-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() text: string;
  @Input() size: btnSize;
  @Input() type: buttonType;
  @Input() disabled: boolean;
  @Output() onClick = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit() {}

  getClasses() {
    const { size, type } = this;
    let className = 'paper-btn';

    if (size) {
      className = `${className} btn-${size}`;
    }
    if (type) {
      className = `${className} btn-${type}`;
    }

    return className;
  }
}

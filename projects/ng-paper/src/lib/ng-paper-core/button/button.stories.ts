import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { ButtonComponent as Button } from './button.component';
import { moduleMetadata } from '@storybook/angular';

export default {
  title: 'Paper Button',
  component: Button,
  decorators: [
    moduleMetadata({
      declarations: [Button]
    })
  ]
};

export const Basic = () => ({
  component: Button,
  props: {
    text: 'Basic Paper Button',
    onClick: action('button clicked')
  }
});

Basic.story = {
  parameters: {
    notes: `<ng-paper-button [text]="Basic Paper Button"></ng-paper-button>`
  }
};

export const Sizes = () => ({
  template: `
        <div style="display: flex;justify-content:space-around;margin: 20px">
            <ng-paper-button [size]="'small'" [text]="'Small'"></ng-paper-button>
            <ng-paper-button [text]="'Medium'"></ng-paper-button>
            <ng-paper-button [size]="'large'" [text]="'Large'"></ng-paper-button>
        </div>
        <ng-paper-button [size]="'block'" [text]="'Block'"></ng-paper-button>
  `
});

export const Colors = () => ({
  template: `
  <ng-paper-button [size]="'large'" [text]="'Primary'"  type="primary"></ng-paper-button>
  <br/><br/>
  <ng-paper-button [size]="'large'" [text]="'Secondary'"  type="secondary"></ng-paper-button>
  <br/><br/>
  <ng-paper-button [size]="'large'" [text]="'Success'"  type="success"></ng-paper-button>
  <br/><br/>
  <ng-paper-button [size]="'large'" [text]="'Warning'"  type="warning"></ng-paper-button>
  <br/><br/>
  <ng-paper-button [size]="'large'" [text]="'Danger'"  type="danger"></ng-paper-button>
  <br/><br/>
  <ng-paper-button [size]="'large'" [text]="'Muted'"  type="muted"></ng-paper-button>
  `
});

export const Disabled = () => ({
  component: Button,
  props: {
    text: 'Disabled',
    size: 'large',
    disabled: true
  }
});

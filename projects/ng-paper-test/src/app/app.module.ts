import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgPaperCoreModule } from 'ng-paper';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, NgPaperCoreModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

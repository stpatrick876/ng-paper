module.exports = {
  stories: ["../projects/ng-paper/src/lib/**/*.stories.ts"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-notes",
    "@storybook/addon-backgrounds"
  ]
};

// ../src/**/*.stories.ts

import { addParameters } from "@storybook/angular"; // <- or your storybook framework

addParameters({
  backgrounds: [
    { name: "plain", value: "#fff", default: true },
    { name: "twitter", value: "#00aced" },
    { name: "facebook", value: "#3b5998" }
  ]
});
